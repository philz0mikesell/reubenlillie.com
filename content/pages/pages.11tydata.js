/**
 * @file Contains metadata common to all pages, to reduce repetition
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Directory data module
 * @module content/pages
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in 11ty}
 */
module.exports = {
  layout: 'layouts/content',
  permalink: '/{{page.fileSlug}}/index.html',
  tags: 'pages'
}
