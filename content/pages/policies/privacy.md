---
title: Privacy Policy
shortTitle: Privacy
weight: 2
description: What kind of data does my site collect from you and why? Spoiler alert—I’ll never even know you’ve been on my site unless you contact me directly.
---

I value my privacy. I value your privacy even more.

This privacy policy applies to all information collected through my site ([https://reubenlillie.com/](https://reubenlillie.com/)).

## What Information Do My Sites Collect?

My sites do not collect any personal information other than what you explicitly share with me through those sites.

For example, on [my contact form][contact] ([https://reubenlillie.com/contact/][contact]), I ask for two pieces of personal information. First, I ask for your first and last name (how you would like me to address you). Second, I ask for a valid reply email address that you will actually access and use to communicate with me.

Unless you contact me to tell me you’ve visited my site, I have no way of knowing.

## How Do I Use Information Collected through My Sites?

I only use personal information you share with me through my sites to communicate directly with you.

## What Information Do My Sites Share?

Whenever you share personal information with me through my sites, I will not share that information with anyone apart from your knowledge and without your explicit permission.

## What If I Want to Share Information Collected through My Sites?

Before sharing any personal information you have shared with me through my sites, I will seek your permission directly. First, I will contact you either in person or by e-mail, phone, private message through a common social media platform, or some other communication method. I will ask you if and how I may share specific personal information you have shared with me through my sites. Then, only after you have given me your explicit permission, will I share any of that information.

## Changes to This Policy

I may update this privacy policy from time to time. I will not reduce your rights under this privacy policy without your explicit consent. I always indicate the date the last changes were published, and I offer access to archived versions for your review. If I make any significant changes, I will provide a more prominent notice.

Reuben L. Lillie

[Contact Me][contact]

[contact]: /contact/
