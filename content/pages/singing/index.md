---
title: Singing
tags: activities
weight: 1
description: I’m an operatic tenor, but I played a lower voice type or two in a former life.
---

The stage is my natural habitat. I feel most alive when I’m telling stories through music.

Depending on how we met, you either know already or else you may have trouble believing that I’m a consummate introvert. That is, I gain most of my energy in solitude—_but the stage is where I release it_. And my favorite kind of music to make is with other people.

I’ve been fortunate throughout my life to have had teachers and mentors who’ve encouraged me to be a versatile singer.

Growing up, I had a steady and rather eclectic diet of classical music, jazz, southern gospel, steel pan, Broadway show tunes, Christmas songs year round, and vintage comedy sketches on vinyl.

I fell in love with stagecraft through musical theater and the silver screen. And I cherish the open arms with which the operatic world has embraced me.

[Have voice, will travel](/contact/).
