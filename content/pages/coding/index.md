---
title: Coding
tags: activities
syntaxHighlighting: true
weight: 4
description: I taught myself how to code in 2014. Now I teach ethical web development at the college level. Let’s make something worthwhile together!
---

```js
var introduce = function (name) {
	return 'Hi, I’m ' + name + '.'
}

introduce('Reuben')
```

I taught myself how to code in the summer of 2014. I was between semesters in seminary, serving an opera contract away from home. The rehearsal schedule was grueling, and I experienced some pretty impressive insomnia. I desperately needed an activity to help me wind down from [singing](/singing/)—something quiet yet productive, still creative yet altogether different.

Since then, I’ve also delved into graphic design and branding practices. I specialize in web ethics: building fast, lightweight, accessible, and secure sites and applications. I’m an outspoken advocate for vanilla JavaScript and JAMstack techniques, helping to found the [JAMstack Chicago MeetUp®](https://www.netlify.com/). And I’ve created an interdisciplinary course for the Olivet Nazarene University Department of Computer Science and Emerging Technologies called “[Ethical Web Development](/comp-496/).”

This site, for example, is built with the static site generator [Eleventy](https://11ty.dev/) and hosted by [Netlify](https://www.netlify.com/). Feel free to check out the [documentation pages](/docs/) and [source code on GitLab](https://gitlab.com/reubenlillie/reubenlillie.com/).

[Let’s make something worthwhile together](/contact/).
