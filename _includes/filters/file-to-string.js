/**
 * @file Defines a filter to convert a file’s contents to a string
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/filters/ Filters in 11ty}
 */

/*
 *Import Node.js native fs module for interacting with the file system
 */
var fs = require('fs')

/**
 * An Eleventy filter for stringifying a file
 * @module _includes/filters/file-to-string
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Converts a file’s contents to a string
   * @param {String} file The path of the file to convert
   *                      (set relative to the input directory, `~/`,
   *                      for consistency with other 11ty features)
   * @return {String} The file’s contents
   * @example `${this.fileToString('includes/assets/css/inline.css')}`
   */
  eleventyConfig.addFilter('fileToString', function (file) {
    return fs.readFileSync(`${file}`).toString()
  })

}
