/**
 * @file Defines the chained template for repertoire
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/content'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ Shortcodes in 11ty}
 */
exports.render = function (data) {
  return `${data.content}
    <section>
      <header>
        <h2>Stage Repertoire <a id="stage">#</a></h2>
      </header>
      <p>Here’s a sample of opera and musical theater roles I’m prepared to perform.</p>
      <p><em>Arranged alphabetically by composer</em></p>
      ${Object.values(data.singing.repertoire.stage).map(composer => `
        <h3>${composer.name}</h3>
        <ul>
          ${composer.works.map(work => `
            <li>
              <em>${work.title}</em>
              (${work.roles.map(role => `${role}`).join(', ')})
            </li>
          `).join('')}
        </ul>`
      ).join('')}
    </section>
    <section>
      <header>
        <h2>Concert Repertoire <a id="concert">#</a></h2>
      </header>
      <p>Here’s a sample of songs, cycles, cantatas, oratorios, and other works for solo voice I’m prepared to present in recital or concert.</p>
      <p><em>Arranged alphabetically by composer</em></p>
      ${Object.values(data.singing.repertoire.concert).map(composer => `
        <h3>${composer.name}</h3>
        <ul>
          ${composer.works.map(work => `
            <li>
              ${work.title.charAt(0) === '“'
                ? `${work.title}${work.largerWork
                    ? ` from <em>${work.largerWork}</em>`
                    : ''
                  }${work.opusNumber
                    ? `, ${work.opusNumber}`
                    : ''
                  }`
                : `<em>${work.title}</em>${work.opusNumber
                    ? `, ${work.opusNumber}`
                    : ''
                  }`
              }
            </li>
          `).join('')}
        </ul>`
      ).join('')}
    </section>`
}
