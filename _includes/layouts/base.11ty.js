/**
 * @file Defines the base template
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#function JavaScript functions as templates in 11ty}
 */

/**
 * Base JavaScript Template module
 * @module _includes/layouts/base
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/data/ Using data in 11ty}
 * @see {@link https://www.11ty.dev/docs/shortcodes/ Shortcodes in 11ty}
 */
module.exports = function (data) {
  return `<!DOCTYPE html>
  <html lang="en">
    ${this.headTag(data)}
    <body class="grid ${data.page.fileSlug ? data.page.fileSlug : 'home'} no-margin">
      ${this.siteHeader(data)}
      <main id="main" class="flex flex-column grid-column-content padding">
        ${data.content}
      </main>
      ${this.siteFooter(data)}
    </body>
  </html>`
}
