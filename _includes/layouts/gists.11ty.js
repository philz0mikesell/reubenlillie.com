/**
 * @file Defines the chained template for coding samples
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/content'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ Shortcodes in 11ty}
 */
exports.render = function (data) {
  return `${data.coding.samples.map(project => `<p>Here’s a sample of code snippets I’ve cooked up.</p>
    <p><em>Arranged by name</em></p>
    ${project.name ? `<h2>${project.name}</h2>` : ''}
    ${project.url ? `<p><a href="${project.url}">${project.url}</a></p>` : ''}
    ${project.description ? `<p>${project.description}</p>` : ''}`
  ).join('')}`
}
