/**
 * @file Defines the chained template for course syllabi
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates.
 */
exports.data = {
  layout: 'layouts/content'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ Shortcodes in 11ty}
 */
exports.render = function (data) {
  return  `<p>Here are some syllabi from university courses I have created and taught.</p>
  <p><em>Arranged by course number</em></p>
  <ul class="hanging-indent no-list-style">
    ${data.teaching.syllabi.map(syllabi => `<li>
      ${syllabi.courseNumber ? `${syllabi.courseNumber}:` : ''}
      ${syllabi.title
        ? syllabi.url
          ? `“<a href="${syllabi.url}">${syllabi.title}</a>.”`
          : `${syllabi.title}.”`
        : ''}
      ${syllabi.institution ? `${syllabi.institution},` : ''}
      ${syllabi.school ? `${syllabi.school},` : ''}
      ${syllabi.department ? `${syllabi.department}.` : ''}
    </li>`
    ).join('')}
  </ul>`
}
