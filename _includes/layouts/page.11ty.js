/**
 * @file Defines the chained template for a basic page
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/base'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 */
exports.render = function (data) {
  return `<article>
      <header id="article_header"
              class="flex flex-column align-items-center text-center">
        <h1>${data.title}</h1>
      </header>
      ${data.content}
    </article>`
}
