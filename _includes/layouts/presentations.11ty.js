/**
 * @file Defines the chained template for presentations
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/content'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ Shortcodes in 11ty}
 */
exports.render = function (data) {
  return  `<p>Here’s a sample of presentations I’ve given at universities, conferences, houses of worship, and MeetUps.</p>
    <p><em>Arranged by date</em></p>
    <ul class="hanging-indent no-list-style">
      ${data.speaking.presentations.map(talk => `<li>
        ${talk.title ? `“${talk.title}.”` : ''}
        ${talk.format ? `${talk.format}.` : ''}
        ${talk.sponsor ? `${talk.sponsor},` : ''}
        ${talk.location ? `${talk.location},` : ''}
        ${talk.date ? `${talk.date}.` : ''}
        ${talk.url ? `<a href="${talk.url}">${talk.url}</a>.` : ''}
      </li>`
      ).join('')}
    </ul>`
}
