/**
 * @file Defines the chained template for basic page content
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/base'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 */
exports.render = function (data) {
  var hasTags = function (obj) {
    return obj.hasOwnProperty('tags')
      ? tag = obj.tags[0]
      : tag = null
  }
  var tag = hasTags(data)
  return `<header>
    <h1 class="flex justify-center no-margin">${data.site.title}</h1>
  </header>
  <aside>
    <div class="x-large">
      ${this.gridNav(data.collections.activities, data.page)}
    </div>
    ${tag !== null
      ? `<div class="italic">
        ${tag !== 'activities'
          ? this.gridNav(data.collections[tag], data.page)
          : this.gridNav(data.collections[data.page.fileSlug], data.page)
        }
      </div>`
      : ''
    }
  </aside>
  <article>
    <header id="article_header"
            class="flex flex-column align-items-center text-center">
      <h1>${data.title}</h1>
    </header>
    ${data.content}
    ${tag !== null
      ? !data.tags.indexOf('policies')
        ? `<p class="small page-date">
            <span>Updated:&nbsp;</span>
            <span class="bold">${this.pageDate(data)}</span
        </p>`
        : ''
      : ''
    }
  </article>`
}
