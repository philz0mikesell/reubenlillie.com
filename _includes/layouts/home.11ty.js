/**
 * @file Defines the chained template for the homepage
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/base',
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ Shortcodes in 11ty}
 */
exports.render = function (data) {
  return `<header id="main_header" class="grid justify-center text-center">
    <h1 class="no-margin">${data.title}</h1>
    <picture style="shape-outside:circle(50%);">
      <source srcset="/_includes/assets/img/headshot.webp" type="image/webp">
      <source srcset="/_includes/assets/img/headshot.jpg" type="image/jpeg">
      <img src="/_includes/assets/img/headshot.jpg" class="circular headshot" alt="Reuben’s headshot" width="300" height="300">
    </picture>
    <p>${data.site.tagline}</p>
  </header>
  <aside class="x-large">
    ${this.gridNav(data.collections.activities, data.page)}
  </aside>
  ${data.content}`
}
