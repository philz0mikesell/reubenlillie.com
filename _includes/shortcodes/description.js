/**
 * @file Defines a shortcode for the description metadata
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for the description metadata
 * @module _includes/shortcodes/description
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * HTML description
   * @method
   * @name titleTag
   * @param {Object} data 11ty’s data object
   * @return {String} HTML template literal
   * @example `${this.description(data)}`
   */
  eleventyConfig.addShortcode('description', function (data) {
    return `<meta name="description"
      content="
      ${data.description
        ? data.description
        : data.site.description
          ? data.site.description
          : data.pkg.description
      }"
    >`
  })

}
