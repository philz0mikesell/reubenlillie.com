/**
 * @file Defines a universal shortcode for social media metadata
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/shortcodes/ 11ty shortcodes}
 */

/**
 * An 11ty shortcode for social media metadata in HTML `<head>` element
 * @module _includes/shortcodes/social-meta
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * OpenGraph and Twitter metadata with fallbacks (~/_data/site.js)
   * @method
   * @name socialMeta
   * @example `${this.socialMeta()}`
   * @param {Object} data 11ty’s data object
   * @return {String} Composite HTML template literal
   * @see {@link https://css-tricks.com/essential-meta-tags-social-media/ Adam Coti, “The Essential Meta Tags for Social Media,” _CSS-Tricks_ (updated December 21, 2016)}
   */
  eleventyConfig.addShortcode('socialMeta', function (data) {
    var html = '';
    html += (data.title)
      ? `<meta property="og:title" content="${data.title}">
        <meta name="twitter:title" content="${data.title}">`
      : `<meta property="og:title" content="${data.site.title}">
        <meta name="twitter:title" content="${data.site.title}">`
    html += (data.description)
      ? `<meta property="og:description" content="${data.description}">
        <meta property="twitter:description" content="${data.description}">`
      : `<meta property="og:description" content="${data.pkg.description}">
        <meta property="twitter:description" content="${data.pkg.description}">`
    html+= (data.thumbnail)
      ? `<meta property="og:image" content="${data.pkg.url}_includes/assets/img/${data.thumbnail}">
        <meta name="twitter:image" content="${data.pkg.url}_includes/assets/img/${data.thumbnail}">
        <meta name="twitter:card" content="summary_large_image">`
      : `<meta property="og:image" content="${data.pkg.url}_includes/assets/img/headshot.jpg">
        <meta name="twitter:image" content="${data.pkg.url}_includes/assets/img/headshot.jpg">
        <meta name="twitter:card" content="summary_large_image">`
    html += `<meta property="og:url" content="${data.page.url}">`
    return html
  })

}
