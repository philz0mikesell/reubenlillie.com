/**
 * @file Defines a shortcode to load a favicon for the user’s platform
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * An 11ty univeral shortcode
 * @module _includes/shortcode/favicon
 * @param {Object} eleventyConfig 11ty’s Config API
 * @see {@link https://www.11ty.dev/docs/filters/ Filters in 11ty}
 */
module.exports = function (eleventyConfig) {

  /**
   * Load the appropriate favicon
   * @method
   * @name favicon
   * @param {Object} data 11ty’s data object
   * @return {String} HTML template literal
   * @example `${this.favicon(data)}`
   * @see {@link https://realfavicongenerator.net/ Favicon Generator}
   */
  eleventyConfig.addShortcode('favicon', function (data) {
    var version = '69BJWKGo25'
    return `<link rel="apple-touch-icon" sizes="180x180" href="/_includes/favicons/apple-touch-icon.png?v=${version}">
      <link rel="icon" type="image/png" sizes="32x32" href="/_includes/favicons/favicon-32x32.png?v=${version}">
      <link rel="icon" type="image/png" sizes="16x16" href="/_includes/favicons/favicon-16x16.png?v=${version}">
      <link rel="manifest" href="/_includes/favicons/site.webmanifest?v=${version}">
      <link rel="mask-icon" href="/_includes/favicons/safari-pinned-tab.svg?v=${version}" color="#101820">
      <link rel="shortcut icon" href="/_includes/favicons/favicon.ico?v=${version}">
      <meta name="apple-mobile-web-app-title" content="${data.site.author.fullName}">
      <meta name="application-name" content="${data.site.author.fullName}">
      <meta name="msapplication-TileColor" content="#ce7639">
      <meta name="msapplication-config" content="/_includes/favicons/browserconfig.xml?v=${version}">
      <meta name="theme-color" content="${data.site.color.primary}">`
  })

}
