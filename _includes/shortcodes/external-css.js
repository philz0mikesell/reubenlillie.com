/**
 * @file Defines a shortcode for loading external stylesheets
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for the external CSS `links`
 * @module _includes/shortcodes/external-css
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * HTML `<head>` markup
   * @method
   * @name externalCSS
   * @param {Object} data 11ty’s data object
   * @return {String} An HTML template literal
   * @example `${this.externalCSS(data)}`
   */
  eleventyConfig.addShortcode('externalCSS', function (data) {
    var html = '';
    (data.page.fileSlug && data.page.fileSlug !== undefined)
      ? html += `<link href="/_includes/assets/css/base.css" rel="stylesheet" media="screen">`
    /* Uncomment this line when there are styles in home.css
      : html += `<link href="/_includes/assets/css/home.css" rel="stylesheet" media="screen">`;
    */
      : html += '';
    (data.form && data.form !== undefined)
      ? html += `<link href="/_includes/assets/css/forms.css" rel="stylesheet" media="screen">`
      : html += '';
    (data.syntaxHighlighting && data.syntaxHighlighting !== undefined)
      ? html += `<link href="/_includes/assets/css/syntax-highlighting.css" rel="stylesheet" media="screen">`
      : html += '';
    return html
  })

}
