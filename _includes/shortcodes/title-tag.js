/**
 * @file Defines a shortcode for the `<title>` markup
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for the `<title>`
 * @module _includes/shortcodes/title-tag
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * HTML `<title>` markup
   * @method
   * @name titleTag
   * @param {Object} data 11ty’s data object
   * @return {String} HTML template literal
   * @example `${this.titleTag(data)}`
   */
  eleventyConfig.addShortcode('titleTag', function (data) {
    return `<title>
      ${data.page.fileSlug
        ? data.site.author.name.givenName + '’s ' + data.title
        : data.title
      }
    </title>`
  })

}
