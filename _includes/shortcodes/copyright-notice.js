/**
 * @file Defines a shortcode for the copyright notice markup
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for the copyright notice
 * @module _includes/shortcodes/copyright-notice
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Copyright notice markup
   * @method
   * @name copyrightNotice
   * @param {Object} 11ty’s data object
   * @return {String} HTML template literal
   * @example `${this.copyrightNotice(data)}`
   */
  eleventyConfig.addShortcode('copyrightNotice', function (data) {
    return `<span id="copyright_year">&copy; ${data.site.copyright.year}</span>
      <span id="copyright_holder">by ${data.site.copyright.holder}.</span>
      <span id="copyright_license">
        <a href="/copyright/"><abbr title="${data.site.copyright.license.abbr}: ${data.site.copyright.license.name}">${data.site.copyright.license.abbr}</abbr></a>.
      </span>`
  })

}
