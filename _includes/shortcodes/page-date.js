/**
 * @file Defines a shortcode for displaying a page’s date
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 * @see {@link https://www.11ty.dev/docs/dates/ Content dates in 11ty}
 */

/**
 * A JavaScript Template module for the `page.date`
 * @module _includes/shortcodes/page-date
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Page date markup
   * @method
   * @name pageDate
   * @param {Object} date The JS `Date` from the 11ty `page` object
   * @return {String} html An HTML template literal
   * @example `${this.pageDate(data.page.date)}`
   */
  eleventyConfig.addShortcode('pageDate', function (data) {
    var date = data.page.date
    var locale = data.site.locale
    var options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    }
    // Check that `date` is a JavaScript `Date` object.
    return Object.prototype.toString.call(date) === "[object Date]"
      ? `${date.toLocaleDateString(locale, options)}`
      : ''
  })

}
