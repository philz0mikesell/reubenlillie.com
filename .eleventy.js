/**
 * @file Configures 11ty options and helper methods
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

// Require 11ty’s syntax highlighting plugin
var syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight")

// Require the theme module
var lloyd = require('./_includes/lloyd.js')

/**
 * 11ty’s configuration module
 * @module .eleventy
 * @param {Object} eleventyConfig 11ty’s Config API
 * @return {Object} {} 11ty’s optional Config object
 * @see {@link https://www.11ty.dev/docs/config/}
 */
module.exports = function (eleventyConfig) {

  // Pass 11ty’s Conif object to the theme configuration file (~/_includes)
  lloyd(eleventyConfig)

  /**
   * Copies static assets to the output directory
   * @see {@link https://www.11ty.dev/docs/copy/ 11ty docs}
   */
  eleventyConfig.addPassthroughCopy('_includes/assets')
  eleventyConfig.addPassthroughCopy('_includes/favicons')

  /**
   * Adds syntax highlighting for code snippets
   * @see {@link https://github.com/11ty/eleventy-plugin-syntaxhighlight Plugin repo on GitHub}
   */
  eleventyConfig.addPlugin(syntaxHighlight, {
    templateFormats: ['md']
  })

}
